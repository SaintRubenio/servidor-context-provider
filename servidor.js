const express = require('express');
const app = express();
const funciones = require('./script.js')
const puerto = 8032;

app.get('/v2/entities', (req, res)=>{
    funciones.buscar().then(response => {
        res.send(response);
    });;
});

app.post('/v2/entities', (req, res)=>{
    funciones.crear().then(response => {
        res.send(response);
    });;
});

app.delete('/v2/entities', (req, res)=>{
    funciones.borrar().then(response => {
        res.send(response);
    });;
});

app.listen(puerto, function(){
    console.log("El servidor está activo en http://localhost:8032/");
});
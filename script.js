const axios = require('axios');

exports.buscar = function(){
    return axios.get('http://82.223.81.195:6601/v2/entities/urn:intecca:Tienda:501?options=keyValues')
    .then(response => {
        return response.data;
    })
    .catch(function (error){
        console.log(error);
        return "No se encontró la tienda.";
    });
}
exports.crear = function(){
    return axios.post('http://82.223.81.195:6601/v2/entities/', {
        "id":"urn:intecca:Tienda:501",
        "type":"Tienda",
        "name":{"type":"Text", "value":"Bricomartin"},
        "address":{"type":"Text", "value": "Calle San Roque"},
        "location":{"type":"Text", "value": "Carracedelo, León"}
    })
    .then(response => {
        return "Creado con éxito.";
    })
    .catch(function (error){
        console.log(error);
        return "Algo salió mal.";
    });
}
exports.borrar = function(){
    return axios.delete('http://82.223.81.195:6601/v2/entities/urn:intecca:Tienda:501')
    .then(response => {
        return "Borrado con éxito.";
    })
    .catch(function (error){
        console.log(error);
        return "Algo salió mal.";
    });
}